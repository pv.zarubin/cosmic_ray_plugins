[![pipeline status](https://gitlab.com/pv.zarubin/cosmic_ray_plugins/badges/master/pipeline.svg)](https://gitlab.com/pv.zarubin/cosmic_ray_plugins/commits/master)

Plugins for Cosmic Ray (python 3)
=================================

1. pip install cosmic-ray-pytest
2. pip install cosmic-ray-localpool
